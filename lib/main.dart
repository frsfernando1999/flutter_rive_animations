import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rive/rive.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const Home(),
    );
  }
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool _play = true;

  bool _lastAnimation1stage = true;
  bool _lastAnimation2stage = false;

  RiveAnimationController? _animation1;
  RiveAnimationController? _animation2;

  Artboard? _riveArtboard;

  @override
  void initState() {
    super.initState();
    rootBundle.load('assets/gear.riv').then((data) async {
      final RiveFile file = RiveFile.import(data);

      _animation1 = SimpleAnimation('spin1');
      _animation2 = SimpleAnimation('spin2');

      final Artboard artboard = file.mainArtboard
        ..addController(_animation2!)
        ..addController(_animation1!);

      setState(() {
        _riveArtboard = artboard;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 100,
              height: 100,
              child: _riveArtboard == null
                  ? SizedBox.shrink()
                  : Rive(
                      artboard: _riveArtboard!,
                    ),
            ),
            ElevatedButton(onPressed: _pausar, child: Text('Pausar')),
            ElevatedButton(onPressed: _girar1, child: Text('Girar 1')),
            ElevatedButton(onPressed: _girar2, child: Text('Girar 2')),
          ],
        ),
      ),
    );
  }

  _girar1() {
    setState(() {
      _animation1?.isActive = true;
      _animation2?.isActive = false;

      _lastAnimation1stage = true;
      _lastAnimation2stage = false;
    });
  }

  _girar2() {
    setState(() {
      _animation1?.isActive = false;
      _animation2?.isActive = true;

      _lastAnimation1stage = false;
      _lastAnimation2stage = true;
    });
  }

  _pausar() {
    setState(() {
      if (_play) {
        _animation1?.isActive = false;
        _animation2?.isActive = false;
        _play = false;
      } else {
        _animation1?.isActive = _lastAnimation1stage;
        _animation2?.isActive = _lastAnimation2stage;
        _play = true;
      }
    });
  }
}

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

